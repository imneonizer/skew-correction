import numpy as np
import argparse
import cv2
import imutils

def add_border(image):
	#adding white padding to each individual cropped character =========================
	shape=image.shape
	w=shape[1]
	h=shape[0]
	base_size=h+50,w+50,3
	#make a 3 channel image for base which is slightly larger than target img
	base=np.zeros(base_size,dtype=np.uint8)
	cv2.rectangle(base,(0,0),(w+50,h+50),(0,0,0),30)#really thick white rectangle
	base[10:h+10,10:w+10]=image
	#====================================================================================
	return base
image = cv2.imread('images/111.jpg')

gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)

blur = cv2.bilateralFilter(gray,9,95,95)

edges = cv2.Canny(blur,50,150,apertureSize = 3)


# Create structure element for extracting horizontal lines through morphology operations
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 1))
# Apply morphology operations
edges = cv2.erode(edges, kernel)

kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 1))
edges = cv2.dilate(edges, kernel)

kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 1))
edges = cv2.erode(edges, kernel)

kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 1))
edges = cv2.dilate(edges, kernel)

#edges = np.dstack([edges])
#print(edges.shape)

contours = cv2.findContours(edges.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
contours = imutils.grab_contours(contours)
mask_lines = np.zeros_like(image)

for c in contours:
	x, y, w, h = cv2.boundingRect(c)
	if w > 200:
		# draw the contour and center of the shape on the image
		cv2.drawContours(mask_lines, [c], -1, (255, 255, 255), 15)

#opening operation
kernel = np.ones((4, 30), np.uint8) 
mask_lines = cv2.morphologyEx(mask_lines, cv2.MORPH_OPEN, kernel, iterations = 1)

#erosion operation
kernel = np.ones((1,50), np.uint8)
mask_lines = cv2.erode(mask_lines, kernel, iterations=1)

#closing operations


mask_lines = add_border(mask_lines)

#cv2.imshow('mask_lines', mask_lines)
cv2.imwrite('images/lines.png', mask_lines)


# convert the image to grayscale and flip the foreground
# and background to ensure foreground is now "white" and
# the background is "black"
#line = cv2.imread('images/lines.png')

gray = cv2.cvtColor(mask_lines, cv2.COLOR_BGR2GRAY)

# threshold the image, setting all foreground pixels to
# 255 and all background pixels to 0
thresh = cv2.threshold(gray, 0, 255,
	cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

# grab the (x, y) coordinates of all pixel values that
# are greater than zero, then use these coordinates to
# compute a rotated bounding box that contains all
# coordinates
coords = np.column_stack(np.where(thresh > 0))
angle = cv2.minAreaRect(coords)[-1]

# the `cv2.minAreaRect` function returns values in the
# range [-90, 0); as the rectangle rotates clockwise the
# returned angle trends to 0 -- in this special case we
# need to add 90 degrees to the angle
if angle < -45:
	angle = -(90 + angle)

# otherwise, just take the inverse of the angle to make
# it positive
else:
	angle = -angle

# rotate the image to deskew it
(h, w) = image.shape[:2]
center = (w // 2, h // 2)
M = cv2.getRotationMatrix2D(center, angle, 1.0)
rotated = cv2.warpAffine(image, M, (w, h),
	flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)

# draw the correction angle on the image so we can validate it
cv2.putText(rotated, "Angle: {:.2f} degrees".format(angle),
	(10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

# show the output image
#print("[INFO] angle: {:.3f}".format(angle))
#cv2.imshow("Input", image)
#cv2.imshow("Rotated", rotated)
cv2.imwrite("Rotated.png", rotated)
#cv2.waitKey(0)